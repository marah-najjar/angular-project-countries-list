import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JsonPipe } from '@angular/common';


@Injectable({
  providedIn: 'root',
})

export class CURDService {

    constructor(private http: HttpClient) { }

    get( url = '', params = {} ) {
        if (!url) return Promise.reject('no url provided');
        return this.http.get(url,params).toPromise();
      }
    
}