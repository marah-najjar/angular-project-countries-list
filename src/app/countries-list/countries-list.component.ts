import { Component, OnInit } from '@angular/core';
import {countriesListService} from './countries-list.service';
import { LocalDataSource } from 'ng2-smart-table';
import {ThemePalette} from '@angular/material/core';
import {ProgressSpinnerMode} from '@angular/material/progress-spinner';

@Component({
  selector: 'app-countries-list',
  templateUrl: './countries-list.component.html',
  styleUrls: ['./countries-list.component.css']
})
export class CountriesListComponent implements OnInit {

  color: ThemePalette = 'primary';
  // indeterminate or determinate
  mode: ProgressSpinnerMode = 'indeterminate';
  value = 50;
  source: LocalDataSource = new LocalDataSource();

  settings = {
    actions: {
          
      add: false,
      edit: false,
      delete: false,
    },
    columns: {
      country: {
        title: 'country',
      },
      domains: {
        title: 'domains'
      },
      web_pages: {
        title: 'web pages'
      },
      alpha_two_code: {
        title: 'alpha two code'
      }
      ,
      name: {
        title: 'name'
      }
      
    }
  };
  data : any;
  constructor(private countriesListService : countriesListService) { }

  ngOnInit(): void {
    this.source.load([]);
    console.log('ssss');
    this.countriesListService.getAll().then(
     result => {
       this.data = result ;
       this.data.map(obj => Object.values(obj)); 
      this.source.load(this.data);
      this.mode = 'determinate';

     } 
    );
  }
   filterdCountry(country) {
    this.mode = 'indeterminate';

     console.log(country.value);
     this.countriesListService.getSelectedCountry(country.value).then(
      (result : any) => {
        result.map(obj => Object.values(obj)); 
       this.source.load(result);
      this.mode = 'determinate';

      } 
     );
  }

}
