// {"country": "United Kingdom", "domains": ["abdn.ac.uk", "aberdeen.ac.uk"], "web_pages": ["http://www.abdn.ac.uk/"], "alpha_two_code": "GB", "name": "University of Aberdeen", "state-province": null},
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import {CURDService} from '../CURDService.service';
@Injectable({
  providedIn: 'root',
})

export class countriesListService {
  baseUrl = 'http://universities.hipolabs.com';
    constructor(private curdService : CURDService) { }

    getAll()  {

      return this.curdService.get(
        this.baseUrl+'/search?name=middle',
           {}
        ).then( result =>   {
         return result;
        });
    }
    getSelectedCountry(countryName)  {

      return this.curdService.get(
        this.baseUrl+'/search?country='+countryName,
           {}
        ).then( result =>   {
         return result;
        });
    }
  }